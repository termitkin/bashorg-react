import React from "react";
import AppRouter from "../AppRouter";
import styled from "@emotion/styled/macro";

const App = () => {
  const Wrapper = styled.div`
    margin-top: 50px;
  `;

  return (
    <Wrapper>
      <AppRouter />
    </Wrapper>
  );
};

export default App;
