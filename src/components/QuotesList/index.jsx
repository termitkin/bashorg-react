import React from "react";
import quotes from "../quotes.json";
import { Link } from "react-router-dom";
import styled from "@emotion/styled/macro";

const QuotesListSortedByDate = props => {
  const Quote = styled.article`
    font-size: 10pt;
    line-height: 1.3em;
    font-family: "Monaco", "Liberation Mono", "Droid Sans Mono", "Ubuntu Mono",
      "Lucida Console", "Helvetica Neue", monospace;
    color: #000;
    min-width: 270px;
    max-width: 740px;
    margin: 30px 15px;

    @media screen and (min-width: 790px) {
      margin: 30px auto;
    }
  `;

  const HeadOfQuote = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    align-items: center;

    @media screen and (min-width: 420px) {
      justify-content: space-between;
    }
  `;

  const QuoteLikes = styled.div`
    width: 116px;
    display: flex;
    justify-content: space-between;
    padding-left: 3px;
  `;

  const Plus = styled.button`
    width: 26px;
    height: 22px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: bold;
    border: none;
    background-color: transparent;
    cursor: pointer;
    padding-bottom: 1px;

    &:hover {
      background-color: #f3f3f3;
      color: #cc0000;
      border-radius: 3px;
    }
  `;

  const Minus = styled(Plus)`
    padding-bottom: 3px;
  `;

  const LikesCount = styled.div`
    width: 48px;
    height: 22px;
    display: flex;
    justify-content: center;
    align-items: center;
  `;

  const QuoteAddedDate = styled.div`
    height: 22px;
    display: flex;
    align-items: center;
    color: #757575;
    white-space: nowrap;
    margin: 0 15px;
  `;

  const QuoteId = styled(Link)`
    color: #0055cc;
    text-decoration: none;
    margin-right: 0;
    white-space: nowrap;

    @media screen and (min-width: 420px) {
      margin-right: 10px;
    }

    &:hover {
      color: #cc0000;
    }
  `;

  const QuoteBody = styled.div`
    white-space: pre-wrap;
    background-color: #f3f3f3;
    border: 1px dotted #bbbbbb;
    border-radius: 3px;
    padding: 7px 8px 5px 8px;
  `;

  let quotesList = [];

  switch (props.location.pathname) {
    case "/bydate/":
      quotesList = quotes.quotes.sort((a, b) => {
        return Date.parse(b.date) - Date.parse(a.date);
      });
      break;

    case "/bylikes/":
      quotesList = quotes.quotes.sort((a, b) => {
        return b.likes - a.likes;
      });
      break;

    case "/":
      quotesList = quotes.quotes.sort((a, b) => {
        return b.likes - a.likes;
      });
      break;

    default:
      quotesList = quotes.quotes.filter(
        quote => "/" + String(quote.id) === props.location.pathname
      );
  }

  quotesList = quotesList.map(quotes => {
    return (
      <Quote key={quotes.id}>
        <HeadOfQuote>
          <QuoteLikes>
            <Plus aria-label="Лайкнуть">+</Plus>
            <LikesCount aria-label="Количество лайков">
              {quotes.likes}
            </LikesCount>
            <Minus aria-label="Дизлайкнуть">–</Minus>
          </QuoteLikes>
          <QuoteAddedDate aria-label="Дата добавления цитаты">
            {quotes.date} {quotes.time}
          </QuoteAddedDate>
          <QuoteId
            to={"/" + quotes.id}
            aria-label="Ссылка на эту цитату"
            onClick={props.changeSortingOrder}
          >
            {"#" + quotes.id}
          </QuoteId>
        </HeadOfQuote>

        <QuoteBody>{quotes.quote}</QuoteBody>
      </Quote>
    );
  });

  return <main>{quotesList}</main>;
};

export default QuotesListSortedByDate;
