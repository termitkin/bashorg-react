import React from "react";
import { Link } from "react-router-dom";
import styled from "@emotion/styled/macro";

const SortingButton = props => {
  let buttonText = props.pathname === "#/bylikes/" ? "по рейтингу" : "новые";
  let to = props.pathname === "#/bydate/" ? "/bydate/" : "/bylikes/";

  const StyledLink = styled(Link)`
    ${props.currentActive === props.pathname
      ? `color: #333; 
         background-color: #f4efdd;
         border-radius: 3px;
         padding: 0px 5px 2px 5px;
         text-decoration: none;`
      : `color: #0055cc;
         background-color: transparent;
         border-color: #acc5e7;
         border: none;
         cursor: pointer;

         &:hover {
           color: #cc0000;
         }
    `};

    margin-left: 12px;
  `;

  const handleclick = () => {
    props.handleclick(props.pathname);
  };

  return (
    <StyledLink to={to} onClick={handleclick}>
      {buttonText}
    </StyledLink>
  );
};

export default SortingButton;
