import React from "react";
import SortingButton from "../SortingButton";
import styled from "@emotion/styled/macro";

const Nav = styled.nav`
  min-width: 270px;
  max-width: 740px;
  margin: 30px 15px;
  display: flex;

  @media screen and (min-width: 790px) {
    margin: 30px auto;
  }
`;

const SortingButtonsGroup = props => {
  const handleclick = currentPath => {
    props.changeSortingOrder(currentPath);
  };

  return (
    <Nav role="navigation">
      <span>Цитаты:</span>
      <SortingButton
        pathname="#/bylikes/"
        handleclick={handleclick}
        currentActive={props.currentActive}
      />
      <SortingButton
        pathname="#/bydate/"
        handleclick={handleclick}
        currentActive={props.currentActive}
      />
    </Nav>
  );
};

export default SortingButtonsGroup;
