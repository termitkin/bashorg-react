import React, { useState } from "react";
import { HashRouter as Router, Route } from "react-router-dom";
import SortingButtonsGroup from "../components/SortingButtonsGroup";
import QuotesList from "../components/QuotesList";

const AppRouter = () => {
  const [currentPath, setCurrentPath] = useState(
    window.location.hash === "" || window.location.hash === "#/"
      ? "#/bylikes/"
      : window.location.hash
  );

  const changeSortingOrder = currentPath => {
    switch (currentPath) {
      case "#/bylikes/":
        setCurrentPath("#/bylikes/");
        break;

      case "#/bydate/":
        setCurrentPath("#/bydate/");
        break;

      default:
        setCurrentPath(null);
    }
  };

  return (
    <Router>
      <SortingButtonsGroup
        changeSortingOrder={changeSortingOrder}
        currentActive={currentPath}
      />

      <Route
        path="/"
        exact
        render={(props = currentPath) => (
          <QuotesList {...props} changeSortingOrder={changeSortingOrder} />
        )}
      />
      <Route
        path={["/bydate", "/bylikes", "/:id"]}
        render={(props = currentPath) => (
          <QuotesList {...props} changeSortingOrder={changeSortingOrder} />
        )}
      />
    </Router>
  );
};

export default AppRouter;
