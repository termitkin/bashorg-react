## Тренировочная страничка на create-react-app

Вывод цитаток с баша из json файла с сортировкой по дате и количеству лайков. С использованием react-router-dom (HashRouter).
Каждая отдельная цитата также доступна по адресу /#/quote_id

[работающая демка](https://termitkin.gitlab.io/bashorg-react).